using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public static int playerHP;
    public static bool isGameOver;
    public Slider healtheBar;
    //public GameObject bloodOverlay;

    void Start()
    {
        isGameOver = false;
        playerHP = 100;
    }

    // Update is called once per frame
    void Update()
    {
        healtheBar.value = playerHP;
        if (isGameOver)
        {
            SceneManager.LoadScene("Game");
        }
        Debug.Log(healtheBar.value);
    }


    public static void Damage(int damageAmount)
    {
        //bloodOverlay.SetActive(true);
        playerHP -= damageAmount;
        if (playerHP <= 0)
            isGameOver = true;

        //yield return new WaitForSeconds(1f);
        //Debug.Log(playerHP);
        //bloodOverlay.SetActive(false);

    }
}
