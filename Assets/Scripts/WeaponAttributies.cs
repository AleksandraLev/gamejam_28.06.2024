using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAttributies : MonoBehaviour
{
    public int damageAmount = 20;
    public M2 player;
    //[SerializeField] AttributesManager atm;

    private void OnTriggerEnter(Collider other)
    {
        //Destroy(transform.GetComponent<Rigidbody>());
        if (other.tag == "Enemy" && player.isAttacking)
        {
            //transform.parent = other.transform;
            other.GetComponent<Enemy>().TakeDamage(damageAmount);
        }
    }
}
