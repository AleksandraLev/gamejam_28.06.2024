using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScript : MonoBehaviour
{
    public int damageCount = 5;

    //private void OnCollisionEnter(Collision collision)
    //{
    //    PlayerManager.Damage(damageCount);
    //}
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerManager.Damage(damageCount);
        }
    }
}
