using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class M : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;

    private Vector3 moveDirection;
    private Vector3 velocity;

    [SerializeField] private bool isGrounded;
    [SerializeField] private float groundCheckDistance;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private float gravity;

    [SerializeField] private float jumpHeight;
    [SerializeField] private float rotationSpeed;

    private CharacterController characterController;
    private Animator animator;

    private bool isAttacking;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        isAttacking = false;
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        isGrounded = Physics.CheckSphere(transform.position, groundCheckDistance, groundMask);
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float moveZ = 0;
        float moveX = 0;

        if (!isAttacking)
        {
            moveZ = Input.GetAxis("Vertical");
            moveX = Input.GetAxis("Horizontal");
        }

        moveDirection = new Vector3(moveX, 0, moveZ);

        if (isGrounded)
        {
            if (moveDirection != Vector3.zero && !Input.GetKey(KeyCode.LeftShift))
            {
                Walk();
            }
            else if (moveDirection != Vector3.zero && Input.GetKey(KeyCode.LeftShift))
            {
                Run();
            }
            else if (moveDirection == Vector3.zero)
            {
                Idle();
            }

            moveDirection = moveDirection.normalized * moveSpeed;

            if (Input.GetKey(KeyCode.Space))
            {
                Jump();
            }

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                StartCoroutine(SwordAttack());
            }

            if (Input.GetKeyDown(KeyCode.LeftAlt))
            {
                Roll();
            }
            if (moveDirection != Vector3.zero)
            {
                Quaternion toRotation = Quaternion.LookRotation(moveDirection, Vector3.up);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
            }
        }

        characterController.Move(moveDirection * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;
        characterController.Move(velocity * Time.deltaTime);
    }

    private void Idle()
    {
        animator.SetFloat("animation", 0.0f, 0.1f, Time.deltaTime);
    }

    private void Walk()
    {
        animator.SetFloat("animation", 0.5f, 0.1f, Time.deltaTime);
        moveSpeed = walkSpeed;
    }

    private void Run()
    {
        animator.SetFloat("animation", 1.0f, 0.1f, Time.deltaTime);
        moveSpeed = runSpeed;
    }

    private void Jump()
    {
        animator.SetTrigger("jump");
        velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        animator.SetTrigger("land");
    }

    private IEnumerator SwordAttack()
    {
        isAttacking = true;
        animator.SetTrigger("attack");
        yield return new WaitForSeconds(1.0f);
        isAttacking = false;
    }

    private void Roll()
    {
        animator.SetTrigger("roll");
    }
}
